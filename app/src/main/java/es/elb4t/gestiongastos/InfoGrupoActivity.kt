package es.elb4t.gestiongastos

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.TextInputLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.google.android.gms.appinvite.AppInviteInvitation
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.firebase.appinvite.FirebaseAppInvite
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.Query
import com.google.firebase.functions.FirebaseFunctions
import es.elb4t.gestiongastos.adapter.MiembroAdapter
import es.elb4t.gestiongastos.firebase.FireDB
import es.elb4t.gestiongastos.firebase.FireDB.Companion.db
import es.elb4t.gestiongastos.model.Invitacion
import es.elb4t.gestiongastos.model.User
import es.elb4t.gestiongastos.utils.FieldValidators.listenersTextoVacio
import kotlinx.android.synthetic.main.activity_info_grupo.*
import kotlinx.android.synthetic.main.content_info_grupo.*


class InfoGrupoActivity : AppCompatActivity(), MiembroAdapter.OnMiembroSelectedListener, GoogleApiClient.OnConnectionFailedListener {
    private val TAG: String = InfoGrupoActivity::class.java.simpleName

    private var idGrupo: String = ""
    private var uidPropietario: String = ""
    private var descripcion: String = ""
    private var nombreEdited: Boolean = false
    private var mQuery: Query? = null
    private var mAdapter: MiembroAdapter? = null
    private lateinit var mMiembrosRecycler: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info_grupo)
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val extras = intent.extras
        if (!extras.getString("idGrupo").isNullOrEmpty())
            idGrupo = extras.getString("idGrupo")
        if (!extras.getString("uidPropietario").isNullOrEmpty())
            uidPropietario = extras.getString("uidPropietario")
        if (!extras.getString("descripcion").isNullOrEmpty())
            descripcion = extras.getString("descripcion")
        Log.e("INFO", "idGrupo = $idGrupo - uidProp: $uidPropietario")
        // Firestore get gastos
        mQuery = db.collection(FireDB.usersPath)
                .whereArrayContains("grupos", idGrupo)


        mMiembrosRecycler = findViewById(R.id.mMiembrosGrupoRecycler)
        mAdapter = object : MiembroAdapter(mQuery!!, this@InfoGrupoActivity, applicationContext, uidPropietario) {
            override fun onDataChanged() {
                // Show/hide content if the query returns empty.
                if (itemCount == 0) {
                    Log.e("INFO", "items = 0")
                } else {
                    Log.e("INFO", "items = $itemCount")
                }
            }

            override fun onError(e: FirebaseFirestoreException) {
                // Show a snackbar on errors
                Log.e("INFO", "ERROR FIND DB GASTOS-- ${e.message}")
                Snackbar.make(findViewById(android.R.id.content),
                        "Error: check logs for info.", Snackbar.LENGTH_LONG).show()
            }
        }

        mMiembrosRecycler.adapter = mAdapter
        mMiembrosRecycler.layoutManager = object : LinearLayoutManager(this) {
            override fun canScrollVertically(): Boolean {
                return false
            }
        }

        fabEditDescripcionGrupo.setOnClickListener { view ->
            actualizarNombreGrupo()
        }

        cardAbandonarGrupo.setOnClickListener {
            abandonarGrupo()
        }

        cardEliminarGrupo.setOnClickListener {
            eliminarGrupo()
        }

        bAddMiembro.setOnClickListener { addUsuarioGrupo() }

        iniciarControles()

        FirebaseDynamicLinks.getInstance().getDynamicLink(intent)
                .addOnSuccessListener { data ->
                    if (data == null) {
                        Log.d(TAG, "getInvitation: no data")
                        return@addOnSuccessListener
                    }

                    // Get the deep link
                    var deepLink = data.link

                    // Extract invite
                    var invite = FirebaseAppInvite.getInvitation(data)
                    if (invite != null) {
                        var invitationId = invite.invitationId
                    }
                }
                .addOnFailureListener {
                    Log.w(TAG, "getDynamicLink:onFailure", it)
                }
    }

    fun iniciarControles() {
        eDescripcionGrupo.text = descripcion
        if (FirebaseAuth.getInstance().currentUser?.uid == uidPropietario) {
            cardEliminarGrupo.visibility = View.VISIBLE
            cardAbandonarGrupo.visibility = View.GONE
            fabEditDescripcionGrupo.visibility = View.VISIBLE
            bAddMiembro.visibility = View.VISIBLE
        } else {
            cardEliminarGrupo.visibility = View.GONE
            cardAbandonarGrupo.visibility = View.VISIBLE
            fabEditDescripcionGrupo.visibility = View.GONE
            bAddMiembro.visibility = View.GONE
        }
    }

    fun eliminarGrupo() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.eliminar_grupo))
        builder.setMessage(getString(R.string.eliminar_grupo_pregunta))

        builder.setPositiveButton(getString(android.R.string.yes)) { dialog, which ->
            FireDB().eliminarGrupoGasto(this, idGrupo)
        }
        builder.setNegativeButton(getString(android.R.string.no)) { dialog, which ->
            dialog.cancel()
        }

        val dialog = builder.create()
        dialog.show()
    }

    fun abandonarGrupo() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.abandonar_grupo))
        builder.setMessage(getString(R.string.abandonar_grupo_pregunta))

        builder.setPositiveButton(getString(android.R.string.yes)) { dialog, which ->
            FireDB().abandonarGruposUsuario(this, FirebaseAuth.getInstance().uid!!, idGrupo)
        }
        builder.setNegativeButton(getString(android.R.string.no)) { dialog, which ->
            dialog.cancel()
        }

        val dialog = builder.create()
        dialog.show()
    }

    override fun onMiembroSelected(miembro: DocumentSnapshot) {
        val user = miembro.toObject(User::class.java)
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.expulsar_usuario))
        builder.setMessage(String.format(getString(R.string.expulsar_usuario_pregunta), user?.nombre))

        builder.setPositiveButton(getString(android.R.string.yes)) { dialog, which ->
            FireDB().expulsarUsuarioGrupo(this, user?.uid!!, idGrupo)
        }
        builder.setNegativeButton(getString(android.R.string.no)) { dialog, which ->
            dialog.cancel()
        }

        val dialog = builder.create()
        dialog.show()
    }

    fun addUsuarioGrupo() {
        val layoutInflaterAndroid = LayoutInflater.from(this)
        val mView = layoutInflaterAndroid.inflate(R.layout.dialog_add_usuario_grupo, null)
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.introduce_email_dialogo_invitacion))
        builder.setView(mView)

        val emailUserDialogo = mView.findViewById(R.id.eEmailUsuarioDialog) as EditText
        val tEmailUserDialogo = mView.findViewById(R.id.tEmailUsuarioDialog) as TextInputLayout
        listenersTextoVacio(emailUserDialogo, tEmailUserDialogo, getString(R.string.campo_vacio))
        builder.setCancelable(false)
                .setPositiveButton(getString(android.R.string.ok)) { dialog, which ->
                    if (!emailUserDialogo.text.isNullOrEmpty()) {
                        buscarUsuarioInvitacionGrupo(emailUserDialogo)
                    }
                }
                .setNegativeButton(getString(android.R.string.cancel)) { dialog, which ->
                    dialog.cancel()
                }

        val dialog = builder.create()
        dialog.show()
    }

    fun buscarUsuarioInvitacionGrupo(emailUserDialogo: EditText) {
        db.collection(FireDB.usersPath)
                .whereEqualTo("email", emailUserDialogo.text.toString())
                .get()
                .addOnCompleteListener { it ->
                    if (it.isSuccessful and !it.result.isEmpty) {
                        val user = it.result.first().toObject(User::class.java)
                        Log.e("ADD USER", "DATA: $user")
                        val data = HashMap<String, Any>()
                        data["tokens"] = user.fcmToken
                        data["titulo"] = getString(R.string.invitacion_unirse_titulo)
                        data["msg"] = String.format(getString(R.string.invitacion_unirse_msg), FirebaseAuth.getInstance().currentUser?.displayName, descripcion)
                        data["uid"] = user.uid
                        data["idGrupo"] = idGrupo
                        FirebaseFunctions.getInstance()
                                .getHttpsCallable("enviarInvitacionGrupo")
                                .call(data)
                                .continueWith {
                                    Log.e("", "Result data: ${it.result.data}")
                                    when (it.result.data.toString()) {
                                        "true" -> Toast.makeText(this, getString(R.string.invitacion_result_ok), Toast.LENGTH_SHORT).show()
                                        "no tokens" -> Toast.makeText(this, getString(R.string.email_no_tokens), Toast.LENGTH_SHORT).show()
                                        else -> Toast.makeText(this, getString(R.string.invitacion_result_error), Toast.LENGTH_SHORT).show()
                                    }
                                }
                    } else {
                        Snackbar.make(findViewById(R.id.include),
                                getString(R.string.email_no_registrado_invitar), Snackbar.LENGTH_LONG)
                                .setAction(getString(R.string.email_no_registrado_snakbar_action)) {
                                    val intent = AppInviteInvitation.IntentBuilder(getString(R.string.app_invite_titulo))
                                            .setMessage(String.format(getString(R.string.app_invite_msg), FirebaseAuth.getInstance().currentUser?.displayName))
                                            .setDeepLink(Uri.parse(getString(R.string.app_invite_deeplink)))
                                            .setCallToActionText(getString(R.string.app_invite_action))
                                            .build()
                                    intent.putExtra("idGrupoInvitacion", idGrupo)
                                    startActivityForResult(intent, 7)
                                }
                                .show()
                    }
                }
    }

    private fun actualizarNombreGrupo() {
        val layoutInflaterAndroid = LayoutInflater.from(this)
        val mView = layoutInflaterAndroid.inflate(R.layout.dialog_crear_grupo, null)
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.introduce_nombre_grupo))
        builder.setView(mView)

        val nombreGrupoDialogo = mView.findViewById(R.id.eNombreGrupoDialog) as EditText
        val tnombreGrupoDialogo = mView.findViewById(R.id.tNombreGrupoDialog) as TextInputLayout
        nombreGrupoDialogo.setText(eDescripcionGrupo.text)
        listenersTextoVacio(nombreGrupoDialogo, tnombreGrupoDialogo, getString(R.string.campo_vacio))
        builder.setCancelable(false)
                .setPositiveButton(getString(android.R.string.ok)) { dialog, which ->
                    if (!nombreGrupoDialogo.text.isNullOrEmpty()) {
                        FireDB().actualizarGrupoGasto(this@InfoGrupoActivity, idGrupo, nombreGrupoDialogo.text.toString())
                        eDescripcionGrupo.text = nombreGrupoDialogo.text.toString()
                        nombreEdited = true
                    }
                }
                .setNegativeButton(getString(android.R.string.cancel)) { dialog, which ->
                    dialog.cancel()
                }

        val dialog = builder.create()
        dialog.show()
    }

    override fun onStart() {
        super.onStart()

        // Start listening for Firestore updates
        if (mAdapter != null) {
            mAdapter?.startListening()
        }
    }

    public override fun onStop() {
        super.onStop()
        if (mAdapter != null) {
            mAdapter?.stopListening()
        }
    }

    override fun onBackPressed() {
        if (nombreEdited) {
            val returnIntent = Intent()
            returnIntent.putExtra("nombreGrupo", eDescripcionGrupo.text)
            setResult(FireDB.RESULT_UPDATE, returnIntent)
            finish()
        } else {
            super.onBackPressed()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.e(TAG, "onActivityResult: requestCode=$requestCode, resultCode=$resultCode, data= $data")

        if (requestCode == 7) {
            if (resultCode == Activity.RESULT_OK) {
                // Get the invitation IDs of all sent messages
                val ids = AppInviteInvitation.getInvitationIds(resultCode, data!!)
                for (id in ids) {
                    Log.d(TAG, "onActivityResult: sent invitation $id")
                    FireDB().insertarInvitacion(id, Invitacion(idGrupo, descripcion, FirebaseAuth.getInstance().currentUser?.displayName!!))
                }
                Toast.makeText(this, getString(R.string.invitacion_result_ok), Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        Log.e(TAG, "ERROR: ${p0.errorMessage}")
    }
}
