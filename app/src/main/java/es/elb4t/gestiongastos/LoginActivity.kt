package es.elb4t.gestiongastos

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.crashlytics.android.Crashlytics
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.ErrorCodes
import com.firebase.ui.auth.IdpResponse
import com.google.firebase.auth.FirebaseAuth
import io.fabric.sdk.android.Fabric
import java.util.*

class LoginActivity : AppCompatActivity() {
    private val RC_SIGN_IN = 123
    var providers = Arrays.asList(
            AuthUI.IdpConfig.EmailBuilder().build(),
            AuthUI.IdpConfig.GoogleBuilder().build())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (!BuildConfig.DEBUG)
            Fabric.with(this, Crashlytics())
        login()
    }

    private fun login() {
        // Si el usuario esta activo en Firebase cargamos la MainActicity sino cargamos el login
        if (FirebaseAuth.getInstance().currentUser != null) {
            val i = Intent(this, MainActivity::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    or Intent.FLAG_ACTIVITY_NEW_TASK
                    or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            if (intent.data != null) {
                i.putExtra("invitacion", intent.data.toString())
            }
            startActivity(i)
        } else {
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders(providers)
                            .setLogo(R.mipmap.ic_launcher_round)
                            .setTheme(R.style.AppTheme)
                            .build(),
                    RC_SIGN_IN)
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val response = IdpResponse.fromResultIntent(data)

            if (resultCode == RESULT_OK) {
                login()
                finish()
            } else {
                when (response?.error?.errorCode) {
                    null -> finish()
                    ErrorCodes.NO_NETWORK -> dialogRed()
                    ErrorCodes.UNKNOWN_ERROR -> {
                        Toast.makeText(this, "Error desconocido", Toast.LENGTH_LONG).show()
                        return
                    }
                    else -> {
                        Toast.makeText(this, "Ha ocurrido un error", Toast.LENGTH_LONG).show()
                        return
                    }
                }
            }
        }
    }

    private fun dialogRed() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.sinConexion))
        builder.setMessage(getString(R.string.noHayConexion))

        val positiveText = getString(R.string.activar)
        builder.setPositiveButton(positiveText
        ) { dialog, which ->
            val settingsIntent = Intent(Settings.ACTION_SETTINGS)
            startActivity(settingsIntent)
        }

        val negativeText = getString(android.R.string.cancel)
        builder.setNegativeButton(negativeText
        ) { dialog, which ->
            finish()
        }

        val dialog = builder.create()
        // display dialog
        dialog.show()
    }
}
