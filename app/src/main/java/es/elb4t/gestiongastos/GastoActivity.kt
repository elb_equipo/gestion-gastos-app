package es.elb4t.gestiongastos

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.DatePicker
import es.elb4t.gestiongastos.firebase.FireDB
import es.elb4t.gestiongastos.model.DataGasto
import es.elb4t.gestiongastos.model.Gasto
import es.elb4t.gestiongastos.utils.FieldValidators
import kotlinx.android.synthetic.main.activity_gasto.*
import kotlinx.android.synthetic.main.content_gasto.*
import java.text.SimpleDateFormat
import java.util.*


class GastoActivity : AppCompatActivity(), DatePickerDialog.OnDateSetListener {

    private val TAG = GastoActivity::class.java.simpleName

    private var idGrupo: String = ""
    private var idGasto: String = ""
    private var inEdition: Boolean = true
    private var isNew: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gasto)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        val extras = intent.extras

        if (!extras.getString("idGrupo").isNullOrEmpty())
            idGrupo = extras.getString("idGrupo")

        if (!extras.getString("id").isNullOrEmpty()) {
            idGasto = extras.getString("id")
            eDescripcionGasto.setText(extras.getString("descripcion"))
            eValorGasto.setText(extras.getDouble("valor").toString())
            eFechaGasto.setText(extras.getString("fecha"))
            inEdition = false
            habilitarEdicion(false)
        } else {
            isNew = true
            habilitarEdicion(true)
        }

        // Listeners para validar los campos de texto
        FieldValidators.listenersTextoVacio(eDescripcionGasto, tDescripcionGasto, getString(R.string.campo_vacio))
        FieldValidators.listenersTextoVacio(eValorGasto, tValorGasto, getString(R.string.campo_vacio))
        FieldValidators.listenersTextoVacio(eFechaGasto, tFechaGasto, getString(R.string.campo_vacio))

        //Mostrar el calendario
        eFechaGasto.setOnFocusChangeListener { _, b -> if (b) mostrarCalendario() }
        eFechaGasto.setOnClickListener { mostrarCalendario() }

        fabGasto.setOnClickListener { view ->
            if (inEdition) {
                guardarGasto()
            } else habilitarEdicion(!inEdition)
        }
    }

    fun mostrarCalendario() {
        val now = Calendar.getInstance()
        val datePickerDialog = DatePickerDialog(
                this,
                this@GastoActivity,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        )
        datePickerDialog.show()
    }

    override fun onDateSet(view: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        eFechaGasto.setText(String.format("%d/%d/%d", dayOfMonth, monthOfYear + 1, year))
    }

    private fun guardarGasto() {
        if (!camposVacios()) {
            if (isNew) {
                FireDB().insertarGasto(this,
                        DataGasto(
                                eDescripcionGasto.text.toString(),
                                eValorGasto.text.toString().toDouble(),
                                SimpleDateFormat("dd/MM/yy", Locale.ENGLISH).parse(eFechaGasto.text.toString()),
                                idGrupo
                        )
                )
            } else {
                FireDB().actualizarGasto(this,
                        Gasto(idGasto, DataGasto(
                                eDescripcionGasto.text.toString(),
                                eValorGasto.text.toString().toDouble(),
                                SimpleDateFormat("dd/MM/yy", Locale.ENGLISH).parse(eFechaGasto.text.toString()),
                                idGrupo
                        )
                        )
                )
            }
        }
    }

    fun habilitarEdicion(editable: Boolean) {
        if (editable)
            fabGasto.setImageResource(R.drawable.ic_save_white)
        else
            fabGasto.setImageResource(R.drawable.ic_edit_white)
        eDescripcionGasto.isEnabled = editable
        eValorGasto.isEnabled = editable
        eFechaGasto.isEnabled = editable
        inEdition = editable
    }

    private fun camposVacios(): Boolean {
        var vacios = eDescripcionGasto.text.toString().isEmpty() or
                eValorGasto.text.toString().isEmpty() or
                eFechaGasto.text.toString().isEmpty()

        // Si el campo esta vacío le añade la cadena vacía para que salte el listener y muestre el error de campo vacío
        if (eDescripcionGasto.text.toString() == "") eDescripcionGasto.setText("")
        if (eValorGasto.text.toString() == "") eValorGasto.setText("")
        if (eFechaGasto.text.toString() == "") eFechaGasto.setText("")

        return vacios
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        if (!isNew)
            menuInflater.inflate(R.menu.menu_gasto, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.eliminarGasto -> {
                eliminarGasto()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun eliminarGasto() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.eliminar_gasto))
        builder.setMessage(getString(R.string.eliminar_gasto_pregunta))

        builder.setPositiveButton(getString(android.R.string.yes)) { dialog, which ->
            FireDB().eliminarGasto(this, idGasto)
        }
        builder.setNegativeButton(getString(android.R.string.no)) { dialog, which ->
            dialog.cancel()
        }

        val dialog = builder.create()
        dialog.show()
    }
}
