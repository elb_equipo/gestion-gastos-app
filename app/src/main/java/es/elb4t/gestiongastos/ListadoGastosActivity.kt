package es.elb4t.gestiongastos

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.Query
import es.elb4t.gestiongastos.adapter.GastosAdapter
import es.elb4t.gestiongastos.firebase.FireDB
import es.elb4t.gestiongastos.model.DataGasto
import es.elb4t.gestiongastos.model.Gasto
import kotlinx.android.synthetic.main.activity_listado_gastos.*
import kotlinx.android.synthetic.main.content_listado_gastos.*
import java.text.SimpleDateFormat
import java.util.*


class ListadoGastosActivity : AppCompatActivity(), GastosAdapter.OnGastoSelectedListener {

    private val TAG: String = ListadoGastosActivity::class.java.simpleName
    private val GASTO_ACTIVITY: Int = 1
    private val INFO_GRUPO_ACTIVITY: Int = 2

    private var idGrupo: String = ""
    private var uidPropietario: String = ""
    private var descripcion: String = ""
    private var mQuery: Query? = null
    private var mAdapter: GastosAdapter? = null
    private lateinit var mGastosRecycler: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listado_gastos)
        setSupportActionBar(toolbar)
        supportActionBar?.title = getString(R.string.title_activity_listado_gastos)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val extras = intent.extras
        if (!extras.getString("idGrupo").isNullOrEmpty())
            idGrupo = extras.getString("idGrupo")
        if (!extras.getString("uidPropietario").isNullOrEmpty())
            uidPropietario = extras.getString("uidPropietario")
        if (!extras.getString("descripcion").isNullOrEmpty()) {
            descripcion = extras.getString("descripcion")
            supportActionBar?.title = descripcion
        }
        Log.e(TAG, "PROPIETARIO: $uidPropietario")

        // Firestore get gastos
        mQuery = FireDB.db.collection(FireDB.gastoPath)
                .whereEqualTo("idGrupo", idGrupo)
                .orderBy("fecha", Query.Direction.DESCENDING)

        mGastosRecycler = findViewById(R.id.recyclerGastos)
        mAdapter = object : GastosAdapter(mQuery!!, this@ListadoGastosActivity) {
            override fun onDataChanged() {
                // Show/hide content if the query returns empty.
                if (itemCount == 0) {
                    mGastosRecycler.visibility = View.GONE
                    noGastosView.visibility = View.VISIBLE
                } else {
                    mGastosRecycler.visibility = View.VISIBLE
                    noGastosView.visibility = View.GONE
                }
            }

            override fun onError(e: FirebaseFirestoreException) {
                // Show a snackbar on errors
                Log.e(TAG, "ERROR FIND DB GASTOS")
                Snackbar.make(findViewById(android.R.id.content),
                        "Error: check logs for info.", Snackbar.LENGTH_LONG).show()
            }
        }

        mGastosRecycler.layoutManager = LinearLayoutManager(this)
        mGastosRecycler.adapter = mAdapter

        fabCrearGasto.setOnClickListener { crearNuevoGasto() }
    }

    private fun crearNuevoGasto() {
        editarGasto(Gasto("", DataGasto(idGrupo)))
    }

    override fun onGastoSelected(gasto: DocumentSnapshot) {
        editarGasto(
                Gasto(
                        gasto.id,
                        gasto.toObject(DataGasto::class.java)!!
                )
        )
    }

    private fun editarGasto(gasto: Gasto) {
        startActivityForResult(
                Intent(applicationContext, GastoActivity::class.java)
                        .putExtra("id", gasto.id)
                        .putExtra("descripcion", gasto.data.descripcion)
                        .putExtra("valor", gasto.data.valor)
                        .putExtra("fecha", SimpleDateFormat("dd/MM/yy", Locale.ENGLISH).format(gasto.data.fecha))
                        .putExtra("idGrupo", gasto.data.idGrupo)
                , GASTO_ACTIVITY)
    }

    override fun onStart() {
        super.onStart()

        // Start listening for Firestore updates
        if (mAdapter != null) {
            mAdapter?.startListening()
        }
    }

    public override fun onStop() {
        super.onStop()
        if (mAdapter != null) {
            mAdapter?.stopListening()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.e(TAG, "REQUESTCODE: $requestCode - RESULCODE: $resultCode")
        when (requestCode) {
            GASTO_ACTIVITY -> when (resultCode) {
                FireDB.RESULT_INSERT -> {
                    Snackbar.make(findViewById(R.id.includeListGastos),
                            String.format(getString(R.string.gasto_creado_ok), data?.getStringExtra("descGasto")), Snackbar.LENGTH_LONG).show()
                }
                FireDB.RESULT_UPDATE -> {
                    if (!data?.getStringExtra("descGasto").isNullOrEmpty()) {
                        Snackbar.make(findViewById(R.id.includeListGastos),
                                String.format(getString(R.string.gasto_actualizado_ok), data?.getStringExtra("descGasto")), Snackbar.LENGTH_LONG).show()
                    }
                }
                FireDB.RESULT_DELETE -> {
                    Snackbar.make(findViewById(R.id.includeListGastos),
                            getString(R.string.gasto_eliminado_ok), Snackbar.LENGTH_LONG).show()
                }
                FireDB.RESULT_ERROR -> {
                    Snackbar.make(findViewById(R.id.includeListGastos),
                            getString(R.string.gasto_creado_error), Snackbar.LENGTH_LONG).show()
                }
            }
            INFO_GRUPO_ACTIVITY -> when (resultCode) {
                FireDB.RESULT_UPDATE -> {
                    if (!data?.getStringExtra("nombreGrupo").isNullOrEmpty()) {
                        descripcion = data?.getStringExtra("nombreGrupo") ?: getString(R.string.title_activity_listado_gastos)
                        supportActionBar?.title = descripcion
                    }
                }
                FireDB.RESULT_DELETE_OR_ABANDONED_GROUP ->{
                    setResult(resultCode, data)
                    finish()
                }
            }
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_listado_gasto, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.infoGrupo -> {
                startActivityForResult(
                        Intent(applicationContext, InfoGrupoActivity::class.java)
                                .putExtra("idGrupo", idGrupo)
                                .putExtra("uidPropietario", uidPropietario)
                                .putExtra("descripcion", descripcion),
                        INFO_GRUPO_ACTIVITY)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}
