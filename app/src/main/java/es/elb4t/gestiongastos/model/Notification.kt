package es.elb4t.gestiongastos.model

import java.util.*

data class Notification(
        val id: String,
        val data: NotificationData
)

data class NotificationData(
        val uid: String,
        val titulo: String,
        val msg: String,
        val idGrupo: String,
        val timestamp: Date,
        val idPushNotify: Int
) {
    constructor() : this("", "", "", "", Date(), 0)
    constructor(uid: String, titulo: String, msg: String, idGrupo: String, idPushNotify: Int) : this(uid, titulo, msg, idGrupo, Date(), idPushNotify)
}

data class Invitacion(
        val idGrupo: String,
        val descripcionGrupo: String,
        val userSendInvitacion: String
) {
    constructor() : this("", "", "")
}