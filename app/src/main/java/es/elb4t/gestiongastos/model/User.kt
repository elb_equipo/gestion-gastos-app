package es.elb4t.gestiongastos.model


data class User(
        var uid: String,
        var nombre: String,
        var email: String,
        var imagen: String,
        var fcmToken: List<String>,
        val grupos: List<String>
) {
    constructor() : this("", "", "", "", emptyList(), emptyList())
    constructor(uid: String, nombre: String, email: String, imagen: String, fcmToken: String) : this(uid, nombre, email, imagen, listOf(fcmToken), emptyList())
}