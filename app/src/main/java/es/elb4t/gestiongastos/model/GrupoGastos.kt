package es.elb4t.gestiongastos.model

import java.util.*


data class GrupoGastos(
        val id: String,
        val data: DataGrupoGastos
)

data class DataGrupoGastos(
        val nombre: String,
        val uidUserPropietario: String,
        val miembros: List<String>
) {
    constructor() : this("", "", emptyList())
}

data class Gasto(
        val id: String,
        val data: DataGasto
)

data class DataGasto(
        val descripcion: String,
        val valor: Double,
        val fecha: Date,
        val idGrupo: String
) {
    constructor() : this("", 0.0, Date(), "")
    constructor(idGrupo: String) : this("", 0.0, Date(), idGrupo)
}