package es.elb4t.gestiongastos.utils

import android.app.Application
import android.content.Context
import android.content.SharedPreferences

class Preferences(app: Application) {
    private var prefs: SharedPreferences = app.getSharedPreferences(app.packageName, Context.MODE_PRIVATE)

    fun putBoolean(name: String, data: Boolean) {
        prefs.edit().putBoolean(name, data).apply()
    }

    fun putInt(name: String, data: Int) {
        prefs.edit().putInt(name, data).apply()
    }

    fun putString(name: String, data: String) {
        prefs.edit().putString(name, data).apply()
    }

    fun getBoolean(name: String): Boolean {
        return prefs.getBoolean(name, false)
    }

    fun getInt(name: String): Int {
        return prefs.getInt(name, 0)
    }

    fun getString(name: String): String {
        return prefs.getString(name, "")
    }

}