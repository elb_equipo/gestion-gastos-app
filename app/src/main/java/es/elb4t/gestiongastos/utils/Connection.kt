package es.elb4t.gestiongastos.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.util.Log
import android.view.View
import com.github.javiersantos.appupdater.AppUpdater
import com.github.javiersantos.appupdater.enums.Display
import com.github.javiersantos.appupdater.enums.Duration
import com.github.javiersantos.appupdater.enums.UpdateFrom
import es.elb4t.gestiongastos.R


/**
 * Created by eloy on 20/1/18.
 */

class Connection(private val ctx: Activity) {
    private val TAG = Connection::class.java.simpleName

    companion object {
        var redActivada: Boolean = false
    }

    /**
     * Metodo para comprobar si hay una nueva actualización
     */
    fun comprobarSiHayActualizacion() {
        val appUpdater = AppUpdater(ctx)
                .setUpdateFrom(UpdateFrom.GOOGLE_PLAY)
                .setDisplay(Display.SNACKBAR)
                .setDuration(Duration.INDEFINITE)
                .setContentOnUpdateAvailable(R.string.actualizacion)
        appUpdater.start()
        Log.e(TAG, "COMPROBANR ACTUALIZACION")
    }

    /**
     * Metodo que comprueba si hay conexión de internet
     */
    fun hayRed(view: View) {
        val connectivityManager = ctx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected)
            redActivada = true
        else {
            Snackbar.make(view,
                    ctx.getString(R.string.noHayConexion),
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(ctx.getString(R.string.activar)) {
                        val settingsIntent = Intent(Settings.ACTION_SETTINGS)
                        ctx.startActivity(settingsIntent)
                    }.show()

            Log.e(TAG, "NO HAY RED ----------------- ")
            redActivada = false
        }
    }
}