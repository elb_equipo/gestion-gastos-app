package es.elb4t.gestiongastos.utils

import android.support.design.widget.TextInputLayout
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText

object FieldValidators {
    fun listenersTextoVacio(editText: EditText, inputText: TextInputLayout, error: String) {
        editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (!p0.isNullOrEmpty()) {
                    inputText.isErrorEnabled = false
                    inputText.error = null
                } else {
                    inputText.isErrorEnabled = true
                    inputText.error = error
                }
            }
        })
    }
}