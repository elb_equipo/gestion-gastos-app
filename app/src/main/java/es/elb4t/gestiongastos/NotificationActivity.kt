package es.elb4t.gestiongastos

import android.app.NotificationManager
import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.Query
import es.elb4t.gestiongastos.adapter.NotificationAdapter
import es.elb4t.gestiongastos.firebase.FireDB
import kotlinx.android.synthetic.main.activity_notification.*
import kotlinx.android.synthetic.main.content_notification.*


class NotificationActivity : AppCompatActivity(), NotificationAdapter.OnNotificationSelectedListener {

    private var mQuery: Query? = null
    private var mAdapter: NotificationAdapter? = null
    private lateinit var mRecycler: RecyclerView
    private lateinit var uidUser: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        uidUser = FirebaseAuth.getInstance().uid!!
        mQuery = FireDB.db.collection(FireDB.notificacionesPath)
                .whereEqualTo("uid", uidUser)
                .orderBy("timestamp", Query.Direction.DESCENDING)

        mRecycler = findViewById(R.id.mNotificationsRecycler)
        mAdapter = object : NotificationAdapter(mQuery!!, this@NotificationActivity) {
            override fun onDataChanged() {
                // Show/hide content if the query returns empty.
                var empty = true
                if (itemCount == 0) {
                    Log.e("INFO", "items = 0")
                } else {
                    Log.e("INFO", "items = $itemCount")
                    empty = false
                }
                cargandoNotificaciones.visibility = View.GONE
                mNotificationsRecycler.visibility = if (empty) View.GONE else View.VISIBLE
                noNotificaciones.visibility = if (empty) View.VISIBLE else View.GONE
            }

            override fun onError(e: FirebaseFirestoreException) {
                // Show a snackbar on errors
                Log.e("INFO", "ERROR FIND DB GASTOS-- ${e.message}")
                Snackbar.make(findViewById(android.R.id.content),
                        "Error: check logs for info.", Snackbar.LENGTH_LONG).show()
                cargandoNotificaciones.visibility = View.GONE
                mNotificationsRecycler.visibility = View.VISIBLE
                noNotificaciones.visibility = View.GONE
            }
        }
        mRecycler.layoutManager = LinearLayoutManager(this)
        mRecycler.adapter = mAdapter
    }

    override fun onNotificationSelected(idGrupo: String, id: String, idPushNotify: Int, invitacionAccept: Boolean) {
        val ctx = this@NotificationActivity
        val nm = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (invitacionAccept) {
            FireDB().addUsuarioGrupo(ctx, idGrupo, uidUser)
            Snackbar.make(ctx.findViewById(R.id.include),
                    getString(R.string.invitacion_acceptada), Snackbar.LENGTH_LONG).show()
        } else {
            Snackbar.make(ctx.findViewById(R.id.include),
                    ctx.getString(R.string.notificacion_eliminada), Snackbar.LENGTH_LONG).show()
        }
        nm.cancel(idPushNotify)
        FireDB().eliminarNotificacion(ctx, id)
    }

    override fun onStart() {
        super.onStart()
        if (mAdapter != null) {
            mAdapter?.startListening()
        }
    }

    public override fun onStop() {
        super.onStop()
        if (mAdapter != null) {
            mAdapter?.stopListening()
        }
    }
}
