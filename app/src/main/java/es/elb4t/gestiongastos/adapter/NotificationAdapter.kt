package es.elb4t.gestiongastos.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.Query
import es.elb4t.gestiongastos.R
import es.elb4t.gestiongastos.model.NotificationData
import kotlinx.android.synthetic.main.cardview_notificacion.view.*


open class NotificationAdapter(query: Query, private val mListener: OnNotificationSelectedListener) :
        FirestoreAdapter<NotificationAdapter.ViewHolder>(query) {

    interface OnNotificationSelectedListener {
        fun onNotificationSelected(idGrupo: String, id: String, idPushNotify: Int, invitacionAccept: Boolean)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.cardview_notificacion, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getSnapshot(position), mListener)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txtTitulo: TextView = itemView.tNotiTitulo
        val txtDescripcion: TextView = itemView.tNotiDescripcion
        val bAceptar: Button = itemView.bNotiAccept
        val bCancelar: Button = itemView.bNotiCancel

        fun bind(snapshot: DocumentSnapshot, listener: OnNotificationSelectedListener?) {

            val notificacion = snapshot.toObject(NotificationData::class.java)
            val resources = itemView.resources

            txtTitulo.text = notificacion?.titulo
            txtDescripcion.text = notificacion?.msg

            // Click listener
            bAceptar.setOnClickListener { listener?.onNotificationSelected(notificacion?.idGrupo!!, snapshot.id, notificacion.idPushNotify, true) }
            bCancelar.setOnClickListener { listener?.onNotificationSelected(notificacion?.idGrupo!!, snapshot.id, notificacion.idPushNotify, false) }
        }
    }
}