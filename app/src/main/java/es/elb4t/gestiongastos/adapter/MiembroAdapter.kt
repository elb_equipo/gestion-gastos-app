package es.elb4t.gestiongastos.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.Query
import es.elb4t.gestiongastos.R
import es.elb4t.gestiongastos.model.User
import kotlinx.android.synthetic.main.cardview_miembro.view.*


open class MiembroAdapter(query: Query, private val mListener: OnMiembroSelectedListener, private val ctx: Context, private val uidPropietario: String) :
        FirestoreAdapter<MiembroAdapter.ViewHolder>(query) {

    interface OnMiembroSelectedListener {
        fun onMiembroSelected(miembro: DocumentSnapshot)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.cardview_miembro, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getSnapshot(position), mListener, ctx, uidPropietario)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txtNombre:TextView = itemView.nombreMiembbro
        val txtEmail:TextView = itemView.emailMiembro
        val imgPerfil: ImageView = itemView.imgPerfil
        val bEliminar: ImageButton = itemView.bEliminarMiembro
        val txtAdmin: TextView = itemView.txtAdmin

        fun bind(snapshot: DocumentSnapshot, listener: OnMiembroSelectedListener?, ctx: Context, uidPropietario: String) {

            val miembro = snapshot.toObject(User::class.java)
            val resources = itemView.resources

            txtNombre.text = miembro?.nombre
            txtEmail.text = miembro?.email
            Glide.with(ctx)
                    .load(miembro?.imagen)
                    .error(Glide.with(ctx)
                            .load(R.mipmap.ic_launcher_round))
                    .into(imgPerfil)

            if ((FirebaseAuth.getInstance().currentUser?.uid == uidPropietario) and
                    (FirebaseAuth.getInstance().currentUser?.uid != miembro?.uid)) {
                bEliminar.visibility = View.VISIBLE
                // Click listener
                bEliminar.setOnClickListener { listener?.onMiembroSelected(snapshot) }
            }else{
                bEliminar.visibility = View.GONE
            }
            if (miembro?.uid == uidPropietario){
                txtAdmin.visibility = View.VISIBLE
            } else {
                txtAdmin.visibility = View.GONE
            }
        }
    }
}