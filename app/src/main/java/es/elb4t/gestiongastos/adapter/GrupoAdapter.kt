package es.elb4t.gestiongastos.adapter

import android.content.res.Resources
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.Query
import es.elb4t.gestiongastos.R
import es.elb4t.gestiongastos.model.DataGrupoGastos
import kotlinx.android.synthetic.main.cardview_grupo.view.*


open class GrupoAdapter(query: Query, private val mListener: OnGroupSelectedListener) :
        FirestoreAdapter<GrupoAdapter.ViewHolder>(query) {

    interface OnGroupSelectedListener {
        fun onGroupSelected(grupo: DocumentSnapshot)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.cardview_grupo, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getSnapshot(position), mListener)

        if (position + 1 == itemCount) {
            // set bottom margin to 72dp.
            setBottomMargin(holder.itemView, (88 * Resources.getSystem().displayMetrics.density).toInt())
        } else {
            // reset bottom margin back to zero. (your value may be different)
            setBottomMargin(holder.itemView, 0)
        }
    }

    fun setBottomMargin(view: View, bottomMargin: Int) {
        if (view.layoutParams is ViewGroup.MarginLayoutParams) {
            val params = view.layoutParams as ViewGroup.MarginLayoutParams
            params.setMargins(params.leftMargin, params.topMargin, params.rightMargin, bottomMargin)
            view.requestLayout()
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textView:TextView = itemView.titulo_grupo
        val imgShared: ImageView = itemView.imgGroupShared

        fun bind(snapshot: DocumentSnapshot, listener: OnGroupSelectedListener?) {

            val grupo = snapshot.toObject(DataGrupoGastos::class.java)
            val resources = itemView.resources

            textView.text = grupo?.nombre
            if (grupo?.miembros?.size!! > 1)
                imgShared.visibility = View.VISIBLE
            else
                imgShared.visibility = View.GONE
            // Click listener
            itemView.setOnClickListener { listener?.onGroupSelected(snapshot) }
        }
    }
}