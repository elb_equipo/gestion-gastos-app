package es.elb4t.gestiongastos.adapter

import android.content.res.Resources
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.Query
import es.elb4t.gestiongastos.R
import es.elb4t.gestiongastos.model.DataGasto
import kotlinx.android.synthetic.main.cardview_gasto.view.*
import java.text.SimpleDateFormat
import java.util.*


open class GastosAdapter(query: Query, private val mListener: OnGastoSelectedListener) :
        FirestoreAdapter<GastosAdapter.ViewHolder>(query) {

    interface OnGastoSelectedListener {
        fun onGastoSelected(gasto: DocumentSnapshot)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.cardview_gasto, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getSnapshot(position), mListener)

        if (position + 1 == itemCount) {
            // set bottom margin to 72dp.
            setBottomMargin(holder.itemView, (88 * Resources.getSystem().displayMetrics.density).toInt())
        } else {
            // reset bottom margin back to zero. (your value may be different)
            setBottomMargin(holder.itemView, 0)
        }
    }

    fun setBottomMargin(view: View, bottomMargin: Int) {
        if (view.layoutParams is ViewGroup.MarginLayoutParams) {
            val params = view.layoutParams as ViewGroup.MarginLayoutParams
            params.setMargins(params.leftMargin, params.topMargin, params.rightMargin, bottomMargin)
            view.requestLayout()
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val descripcion: TextView = itemView.descripcionGasto
        val valor: TextView = itemView.txtValorGasto
        val fecha: TextView = itemView.txtFechaGasto

        fun bind(snapshot: DocumentSnapshot, listener: OnGastoSelectedListener?) {

            val gasto = snapshot.toObject(DataGasto::class.java)
            val resources = itemView.resources
            descripcion.text = gasto?.descripcion
            valor.text = "${gasto?.valor} €"
            fecha.text = SimpleDateFormat("dd/MM/yy", Locale.ENGLISH).format(gasto?.fecha)

            // Click listener
            itemView.setOnClickListener { listener?.onGastoSelected(snapshot) }
        }
    }
}