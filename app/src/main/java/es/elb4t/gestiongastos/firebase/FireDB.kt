package es.elb4t.gestiongastos.firebase

import android.app.Activity
import android.content.Intent
import android.support.design.widget.Snackbar
import android.util.Log
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.iid.FirebaseInstanceId
import es.elb4t.gestiongastos.R
import es.elb4t.gestiongastos.model.*


class FireDB() {
    private val TAG: String = FireDB::class.java.simpleName

    companion object {
        const val RESULT_ERROR: Int = -1
        const val RESULT_INSERT: Int = 1
        const val RESULT_UPDATE: Int = 2
        const val RESULT_DELETE: Int = 3
        const val RESULT_DELETE_OR_ABANDONED_GROUP: Int = 4
        val db: FirebaseFirestore
            get() = FirebaseFirestore.getInstance()
        const val usersPath: String = "users"
        const val gruposPath: String = "grupos"
        const val gastoPath: String = "gastos"
        const val notificacionesPath: String = "notificaciones"
        const val invitacionesPath: String = "invitaciones"
    }

    // USUARIO
    fun insertarUsuario(data: User) {
        db.collection(usersPath)
                .document(data.uid)
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val document = task.result
                        Log.e(TAG, "insertar usuario DOCUMENT EXIST: ${document.exists()}")
                        if (document.exists()) {
                            document.reference
                                    .update(mapOf(
                                            "uid" to data.uid,
                                            "nombre" to data.nombre,
                                            "email" to data.email,
                                            "imagen" to data.imagen))
                                    .addOnSuccessListener { Log.e(TAG, "DocumentSnapshot upload") }
                                    .addOnFailureListener { e -> Log.e(TAG, "Error upload document", e) }
                            actualizarFcmTokenUsuario(data.uid, FirebaseInstanceId.getInstance().token!!)
                            Log.d(TAG, "DocumentSnapshot data: " + document.data!!)
                        } else {
                            Log.d(TAG, "No such document")
                            document.reference
                                    .set(data)
                                    .addOnSuccessListener { Log.e(TAG, "DocumentSnapshot added") }
                                    .addOnFailureListener { e -> Log.e(TAG, "Error adding document", e) }
                        }
                    } else {
                        Log.d(TAG, "get failed with ", task.exception)
                    }
                }
    }

    fun actualizarNombreUsuario(uid: String, nombre: String) {
        db.collection(usersPath)
                .document(uid)
                .update("nombre", nombre)
                .addOnSuccessListener { Log.e(TAG, "DocumentSnapshot updated") }
                .addOnFailureListener { e -> Log.e(TAG, "Error updating document", e) }
    }

    fun actualizarEmailUsuario(uid: String, email: String) {
        db.collection(usersPath)
                .document(uid)
                .update("email", email)
                .addOnSuccessListener { Log.e(TAG, "DocumentSnapshot updated") }
                .addOnFailureListener { e -> Log.e(TAG, "Error updating document", e) }
    }

    fun actualizarFcmTokenUsuario(uid: String, token: String) {
        db.collection(usersPath)
                .document(uid)
                .update("fcmToken", FieldValue.arrayUnion(token))
                .addOnSuccessListener { Log.e(TAG, "DocumentSnapshot updated") }
                .addOnFailureListener { e -> Log.e(TAG, "Error updating document", e) }
    }

    fun eliminarFcmTokenUsuario(uid: String, token: String) {
        db.collection(usersPath)
                .document(uid)
                .update("fcmToken", FieldValue.arrayRemove(token))
                .addOnSuccessListener { Log.e(TAG, "DocumentSnapshot updated") }
                .addOnFailureListener { e -> Log.e(TAG, "Error updating document", e) }
    }

    fun insertarGruposUsuario(uid: String, idGrupo: String) {
        db.collection(usersPath)
                .document(uid)
                .update("grupos", FieldValue.arrayUnion(idGrupo))
                .addOnSuccessListener { Log.e(TAG, "DocumentSnapshot updated") }
                .addOnFailureListener { e -> Log.e(TAG, "Error updating document", e) }
    }

    fun abandonarGruposUsuario(ctx: Activity, uid: String, idGrupo: String) {
        db.collection(usersPath)
                .document(uid)
                .update("grupos", FieldValue.arrayRemove(idGrupo))
                .addOnSuccessListener { Log.e(TAG, "DocumentSnapshot updated") }
                .addOnFailureListener { e -> Log.e(TAG, "Error updating document", e) }
        db.collection(gruposPath)
                .document(idGrupo)
                .update("miembros", FieldValue.arrayRemove(uid))
                .addOnSuccessListener {
                    Log.e(TAG, "DocumentSnapshot updated")
                    val returnIntent = Intent()
                    returnIntent.putExtra("msg", ctx.getString(R.string.grupo_abandonado_ok))
                    ctx.setResult(RESULT_DELETE_OR_ABANDONED_GROUP, returnIntent)
                    ctx.finish()
                }
                .addOnFailureListener { e -> Log.e(TAG, "Error updating document", e) }
    }

    fun expulsarUsuarioGrupo(ctx: Activity, uid: String, idGrupo: String) {
        db.collection(usersPath)
                .document(uid)
                .update("grupos", FieldValue.arrayRemove(idGrupo))
                .addOnSuccessListener { Log.e(TAG, "DocumentSnapshot updated") }
                .addOnFailureListener { e -> Log.e(TAG, "Error updating document", e) }
        db.collection(gruposPath)
                .document(idGrupo)
                .update("miembros", FieldValue.arrayRemove(uid))
                .addOnSuccessListener {
                    Log.e(TAG, "DocumentSnapshot updated")
                    Snackbar.make(ctx.findViewById(R.id.include),
                            ctx.getString(R.string.grupo_usuario_expulsado_ok), Snackbar.LENGTH_LONG).show()
                }
                .addOnFailureListener { e -> Log.e(TAG, "Error updating document", e) }
    }

    fun addUsuarioGrupo(ctx: Activity, idGrupo: String, uid: String) {
        db.collection(gruposPath)
                .document(idGrupo)
                .update("miembros", FieldValue.arrayUnion(uid))
                .addOnSuccessListener {
                    Log.e(TAG, "DocumentSnapshot updated")
                    insertarGruposUsuario(uid, idGrupo)
                }
                .addOnFailureListener { e -> Log.e(TAG, "Error updating document", e) }
    }

    // GRUPOS
    fun insertarGrupoGasto(ctx: Activity, data: DataGrupoGastos) {
        db.collection(gruposPath)
                .add(data)
                .addOnSuccessListener {
                    Log.e(TAG, "Grupo ins >> ID: ${it.id} NOMBRE: ${data.nombre} - PROPIETARIO: ${data.uidUserPropietario}")
                    Log.e(TAG, "DocumentSnapshot added")
                    Snackbar.make(ctx.findViewById(R.id.include),
                            ctx.getString(R.string.grupo_creado_ok), Snackbar.LENGTH_LONG).show()
                    insertarGruposUsuario(data.uidUserPropietario, it.id)
                }
                .addOnFailureListener { e ->
                    Snackbar.make(ctx.findViewById(R.id.include),
                            ctx.getString(R.string.error_crear_grupo), Snackbar.LENGTH_LONG).show()
                    Log.e(TAG, "Error adding document", e)
                }
    }

    fun actualizarGrupoGasto(ctx: Activity, idGrupo: String, nombre: String) {
        db.collection(gruposPath)
                .document(idGrupo)
                .update("nombre", nombre)
                .addOnSuccessListener {
                    Log.e(TAG, "Grupo upd >> IDgrupo: $idGrupo NOMBRE: $nombre")
                    Log.e(TAG, "DocumentSnapshot updated")
                    Snackbar.make(ctx.findViewById(R.id.include),
                            ctx.getString(R.string.nombre_grupo_actualizado), Snackbar.LENGTH_LONG).show()
                }
                .addOnFailureListener { e ->
                    Snackbar.make(ctx.findViewById(R.id.include),
                            ctx.getString(R.string.error_crear_grupo), Snackbar.LENGTH_LONG).show()
                    Log.e(TAG, "Error updating document", e)
                }
    }

    fun eliminarGrupoGasto(ctx: Activity, idGrupo: String) {
        db.collection(gruposPath)
                .document(idGrupo)
                .delete()
                .addOnSuccessListener {
                    Log.d(TAG, "DocumentSnapshot successfully deleted!")
                    val returnIntent = Intent()
                    returnIntent.putExtra("msg", ctx.getString(R.string.grupo_eliminado_ok))
                    ctx.setResult(RESULT_DELETE_OR_ABANDONED_GROUP, returnIntent)
                    ctx.finish()
                }
                .addOnFailureListener { e -> Log.w(TAG, "Error deleting document", e) }
    }

    // GASTOS
    fun insertarGasto(ctx: Activity, data: DataGasto) {
        db.collection(gastoPath)
                .add(data)
                .addOnSuccessListener {
                    Log.e(TAG, "Gasto ins >> ID: ${it.id} NOMBRE: ${data.descripcion} - VALOR: ${data.valor} - idGrupo: ${data.idGrupo}")
                    Log.e(TAG, "DocumentSnapshot added")
                    val returnIntent = Intent()
                    returnIntent.putExtra("descGasto", data.descripcion)
                    ctx.setResult(RESULT_INSERT, returnIntent)
                    ctx.finish()
                }
                .addOnFailureListener { e ->
                    Log.e(TAG, "Error adding document", e)
                    val returnIntent = Intent()
                    ctx.setResult(RESULT_ERROR, returnIntent)
                    ctx.finish()
                }
    }

    fun actualizarGasto(ctx: Activity, gasto: Gasto) {
        db.collection(gastoPath)
                .document(gasto.id)
                .set(gasto.data)
                .addOnSuccessListener {
                    Log.e(TAG, "Gasto updt >> ID: ${gasto.id} NOMBRE: ${gasto.data.descripcion} - VALOR: ${gasto.data.valor} - idGrupo: ${gasto.data.idGrupo}")
                    Log.e(TAG, "DocumentSnapshot added")
                    val returnIntent = Intent()
                    returnIntent.putExtra("descGasto", gasto.data.descripcion)
                    ctx.setResult(RESULT_UPDATE, returnIntent)
                    ctx.finish()
                }
                .addOnFailureListener { e ->
                    val returnIntent = Intent()
                    ctx.setResult(RESULT_ERROR, returnIntent)
                    ctx.finish()
                    Log.e(TAG, "Error updating document", e)
                }
    }

    fun eliminarGasto(ctx: Activity, idGasto: String) {
        db.collection(gastoPath)
                .document(idGasto)
                .delete()
                .addOnSuccessListener {
                    Log.d(TAG, "DocumentSnapshot successfully deleted!")
                    val returnIntent = Intent()
                    ctx.setResult(RESULT_DELETE, returnIntent)
                    ctx.finish()
                }
                .addOnFailureListener { e -> Log.w(TAG, "Error deleting document", e) }
    }

    // NOTIFICACIONES
    fun insertarNotificacion(noti: Notification) {
        db.collection(notificacionesPath)
                .document(noti.id)
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val document = task.result
                        Log.e(TAG, "insertar notificacion DOCUMENT EXIST: ${document.exists()}")
                        if (!document.exists()) {
                            Log.d(TAG, "No such document")
                            document.reference
                                    .set(noti.data)
                                    .addOnSuccessListener { Log.e(TAG, "DocumentSnapshot added") }
                                    .addOnFailureListener { e -> Log.e(TAG, "Error adding document", e) }
                        }
                    } else {
                        Log.d(TAG, "get failed with ", task.exception)
                    }
                }
    }

    fun eliminarNotificacion(ctx: Activity, id: String) {
        db.collection(notificacionesPath)
                .document(id)
                .delete()
                .addOnSuccessListener { Log.d(TAG, "DocumentSnapshot successfully deleted!") }
                .addOnFailureListener { e -> Log.w(TAG, "Error deleting document", e) }
    }

    // INVITACIONES
    fun insertarInvitacion(id: String, data: Invitacion) {
        db.collection(invitacionesPath)
                .document(id)
                .set(data)
                .addOnSuccessListener { Log.e(TAG, "DocumentSnapshot added") }
                .addOnFailureListener { e -> Log.e(TAG, "Error adding document", e) }
    }

    fun eliminarInvitacion(ctx: Activity, id: String) {
        db.collection(invitacionesPath)
                .document(id)
                .delete()
                .addOnSuccessListener { Log.d(TAG, "DocumentSnapshot successfully deleted!") }
                .addOnFailureListener { e -> Log.w(TAG, "Error deleting document", e) }
    }
}