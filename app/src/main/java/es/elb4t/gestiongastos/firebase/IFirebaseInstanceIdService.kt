package es.elb4t.gestiongastos.firebase

import android.util.Log
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService



class IFirebaseInstanceIdService : FirebaseInstanceIdService() {
    private val TAG = IFirebaseInstanceIdService::class.java.simpleName

    override fun onTokenRefresh() {
        val fcmToken = FirebaseInstanceId.getInstance().token
        Log.e(TAG, "--------FCM Token Refresh: " + fcmToken!!)
    }
}