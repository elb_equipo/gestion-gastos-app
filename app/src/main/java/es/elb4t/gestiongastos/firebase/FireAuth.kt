package es.elb4t.gestiongastos.firebase

import android.app.Application
import android.content.Intent
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.iid.FirebaseInstanceId
import es.elb4t.gestiongastos.LoginActivity
import es.elb4t.gestiongastos.utils.Preferences


class FireAuth {
    companion object {

        fun logoutUser(app: Application) {
            FireDB().eliminarFcmTokenUsuario(FirebaseAuth.getInstance().uid!!, FirebaseInstanceId.getInstance().token!!)
            AuthUI.getInstance()
                    .signOut(app.applicationContext)
                    .addOnCompleteListener {
                        Preferences(app).putBoolean("isLogued", false)
                        Thread(Runnable {
                            FirebaseInstanceId.getInstance().deleteInstanceId()
                        }).start()
                        val i = Intent(app.applicationContext, LoginActivity::class.java)
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                                or Intent.FLAG_ACTIVITY_NEW_TASK
                                or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        app.applicationContext.startActivity(i)
                    }
        }
    }

}