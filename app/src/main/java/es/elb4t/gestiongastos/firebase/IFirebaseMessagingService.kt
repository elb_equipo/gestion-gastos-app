package es.elb4t.gestiongastos.firebase

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import es.elb4t.gestiongastos.NotificationActivity
import es.elb4t.gestiongastos.R
import es.elb4t.gestiongastos.model.Notification
import es.elb4t.gestiongastos.model.NotificationData
import java.util.*


class IFirebaseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.e(TAG, "¡Mensaje recibido!")
        Log.e(TAG, "REMOTE MSG: ${remoteMessage.from} -- ${remoteMessage.messageId}")
        Log.e(TAG, "REMOTE DATA MSG: ${remoteMessage.data}")

        if (remoteMessage.data != null) {
            val idNoti = try {
                remoteMessage.from!!.toInt()
            } catch (e: Exception) {
                Random().nextInt()
            }

            val notificacion = Notification(
                    remoteMessage.messageId!!,
                    NotificationData(remoteMessage.data["uid"]!!, remoteMessage.data["titulo"]!!, remoteMessage.data["msg"]!!, remoteMessage.data["idGrupo"]!!, idNoti)
            )

            displayNotification(this, notificacion)
        }
    }

    companion object {
        private val TAG = IFirebaseMessagingService::class.java.simpleName
        val CHANNEL_ID = "inv"

        fun displayNotification(ctx: Context, notificacion: Notification) {
            Log.e("IFirebaseMessagingServi", "REMOTE MSG: ${notificacion.data.titulo} - ${notificacion.data.msg}")
            FireDB().insertarNotificacion(notificacion)

            createNotificationChannel(ctx)

            val intent = Intent(ctx, NotificationActivity::class.java)
            val stackBuilder = TaskStackBuilder.create(ctx)
            stackBuilder.addNextIntentWithParentStack(intent)
            val pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)

            val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val notificationBuilder = NotificationCompat.Builder(ctx, CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_notification)
                    .setLargeIcon(BitmapFactory.decodeResource(ctx.resources, R.mipmap.ic_launcher_round))
                    .setColor(ContextCompat.getColor(ctx, R.color.secondaryColor))
                    .setContentTitle(notificacion.data.titulo)
                    .setStyle(NotificationCompat.BigTextStyle()
                            .bigText(notificacion.data.msg)
                            .setSummaryText(ctx.getString(R.string.app_name)))
                    .setContentText(notificacion.data.msg)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setLights(ContextCompat.getColor(ctx, R.color.secondaryColor), 1000, 500)
                    .setSound(defaultSoundUri)

            val notificationManager = ctx.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            notificationManager.notify(notificacion.data.idPushNotify, notificationBuilder.build())
        }

        fun createNotificationChannel(ctx: Context) {
            // Create the NotificationChannel, but only on API 26+ because
            // the NotificationChannel class is new and not in the support library
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val name = "Invitación"
                val description = "Invitación para un grupo"
                val importance = NotificationManager.IMPORTANCE_HIGH
                val channel = NotificationChannel(CHANNEL_ID, name, importance)
                channel.description = description
                // Register the channel with the system; you can't change the importance
                // or other notification behaviors after this
                val notificationManager = ctx.getSystemService(NotificationManager::class.java)
                notificationManager!!.createNotificationChannel(channel)
            }
        }
    }

}