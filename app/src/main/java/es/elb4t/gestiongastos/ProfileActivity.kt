package es.elb4t.gestiongastos

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.TextInputLayout
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.bumptech.glide.Glide
import com.google.firebase.auth.*
import es.elb4t.gestiongastos.firebase.FireDB
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.content_profile.*


class ProfileActivity : AppCompatActivity() {
    private val TAG: String = ProfileActivity::class.java.simpleName

    private var user: FirebaseUser? = null
    var isEdited: Boolean = false
    var inEdition: Boolean = false
    var isEmailEdited: Boolean = false
    var isPasswEdited: Boolean = false
    var isImageEdited: Boolean = false
    var nombreAnterior: String = ""
    var emailAnterior: String = ""
    var passwordAnterior: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        user = FirebaseAuth.getInstance().currentUser
        loadPerfil()

        fabEditPerfil.setOnClickListener {
            if (inEdition) {
                guardarPerfil()
            } else habilitarEdicion(!inEdition)
        }

        fabImgPerfil.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }

        nombreAnterior = eNombrePerfil.text.toString()
        emailAnterior = eEmailPerfil.text.toString()

        listenersEditText()
    }

    private fun listenersEditText() {
        eNombrePerfil.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(text: Editable?) {
                isEdited = nombreAnterior != text.toString()
                Log.e("IS EDIT nombre", "$isEdited : $nombreAnterior -- $text")
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        })
        eEmailPerfil.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(text: Editable?) {
                isEmailEdited = emailAnterior != text.toString()
                Log.e("IS EDIT email", "$isEmailEdited : $emailAnterior -- $text")
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        })
        ePasswordPerfil.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(text: Editable?) {
                isPasswEdited = passwordAnterior != text.toString()
                Log.e("IS EDIT passw", "$isPasswEdited : $passwordAnterior -- $text")
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        })
    }

    private fun loadPerfil() {
        if (user != null) {
            eNombrePerfil.setText(user?.displayName)
            eEmailPerfil.setText(user?.email)
            Glide.with(applicationContext)
                    .load(user?.photoUrl)
                    .error(Glide.with(applicationContext)
                            .load(R.mipmap.ic_launcher_round))
                    .into(imgPerfil)
            if (!user?.providers?.get(0).equals("password"))
                tPasswordPerfil.visibility = View.GONE
        } else {
            Toast.makeText(applicationContext, getString(R.string.errorCargarPerfil), Toast.LENGTH_LONG).show()
        }
    }

    fun habilitarEdicion(editable: Boolean) {
        if (!isEdited and !isEmailEdited and !isPasswEdited) {
            if (editable)
                fabEditPerfil.setImageResource(R.drawable.ic_save_white)
            else
                fabEditPerfil.setImageResource(R.drawable.ic_edit_white)
            eNombrePerfil.isEnabled = editable
            if (user?.providers?.get(0).equals("password")) {
                eEmailPerfil.isEnabled = editable
                ePasswordPerfil.isEnabled = editable
            }
            inEdition = editable
        }
    }

    fun guardarPerfil() {
        if (user != null) {
            // Guardar nombre
            if (isEdited) {
                val profileUpdates = UserProfileChangeRequest.Builder()
                        .setDisplayName(eNombrePerfil.text.toString())
                        .build()

                user?.updateProfile(profileUpdates)
                        ?.addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                isEdited = false
                                habilitarEdicion(!inEdition)
                                nombreAnterior = eNombrePerfil.text.toString()
                                Toast.makeText(applicationContext, getString(R.string.nombreGuardado), Toast.LENGTH_LONG).show()
                                Log.e(TAG, getString(R.string.nombreGuardado))
                                FireDB().actualizarNombreUsuario(user?.uid!!, eNombrePerfil.text.toString())
                            } else {
                                Toast.makeText(applicationContext, getString(R.string.errorGuardar), Toast.LENGTH_LONG).show()
                                Log.e(TAG, getString(R.string.errorGuardar))
                            }
                        }
            }

            // Modificar email
            if (isEmailEdited) {
                user?.updateEmail(eEmailPerfil.text.toString())
                        ?.addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                isEmailEdited = false
                                habilitarEdicion(!inEdition)
                                emailAnterior = eEmailPerfil.text.toString()
                                Toast.makeText(applicationContext, getString(R.string.email_update_ok), Toast.LENGTH_LONG).show()
                                Log.e(TAG, getString(R.string.email_update_ok))
                                FireDB().actualizarEmailUsuario(user?.uid!!, eEmailPerfil.text.toString())
                            }
                        }
                        ?.addOnFailureListener {
                            Log.e(TAG, "EX: ${it.javaClass.simpleName} - Msg: ${it.message}")
                            when (it) {
                                is FirebaseAuthUserCollisionException -> {
                                    Snackbar.make(includePrefil, getString(R.string.err_email_existente), Snackbar.LENGTH_LONG).show()
                                }
                                is FirebaseAuthInvalidCredentialsException -> {
                                    Snackbar.make(includePrefil, getString(R.string.err_email_mal_formado), Snackbar.LENGTH_LONG).show()
                                }
                                is FirebaseAuthInvalidUserException -> {
                                    Snackbar.make(includePrefil, getString(R.string.err_email_deshabilidato_eliminado), Snackbar.LENGTH_LONG).show()
                                }
                                is FirebaseAuthRecentLoginRequiredException -> dialogPassword("")
                            }
                        }
            }

            // Modificar contraseña
            if (isPasswEdited) {
                tPasswordPerfil.error = ""
                user?.updatePassword(ePasswordPerfil.text.toString())
                        ?.addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                isPasswEdited = false
                                habilitarEdicion(!inEdition)
                                ePasswordPerfil.setText("")
                                Toast.makeText(applicationContext, getString(R.string.password_update_ok), Toast.LENGTH_LONG).show()
                                Log.e(TAG, getString(R.string.password_update_ok))
                            }
                        }
                        ?.addOnFailureListener {
                            Log.e(TAG, "EXC: ${it.javaClass.simpleName} - MSG: ${it.message}")
                            when (it) {
                                is FirebaseAuthWeakPasswordException -> tPasswordPerfil.error = getString(R.string.minima_longitud_password)
                                is FirebaseAuthRecentLoginRequiredException -> dialogPassword("")
                            }
                        }
            }

            if (!isEdited and !isEmailEdited)
                habilitarEdicion(!inEdition)
        }
    }

    fun dialogPassword(error: String) {
        val layoutInflaterAndroid = LayoutInflater.from(this)
        val mView = layoutInflaterAndroid.inflate(R.layout.dialog_insetar_password, null)
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.dialog_verificar_password))
        builder.setView(mView)

        val passwordDialog = mView.findViewById(R.id.ePasswordDialog) as EditText
        val passwordInput = mView.findViewById(R.id.tPasswordDialog) as TextInputLayout
        passwordInput.error = error
        builder.setCancelable(false)
                .setPositiveButton(getString(android.R.string.yes)) { dialogBox, id ->
                    val user = FirebaseAuth.getInstance().currentUser

                    if (!passwordDialog.text.isNullOrBlank() && user != null) {
                        val credential = EmailAuthProvider
                                .getCredential(user.email!!, passwordDialog.text.toString())

                        user.reauthenticate(credential)
                                .addOnCompleteListener {
                                    if (it.isSuccessful) {
                                        guardarPerfil()
                                        Toast.makeText(applicationContext, getString(R.string.password_ok), Toast.LENGTH_LONG).show()
                                    }
                                }
                                .addOnFailureListener {
                                    dialogPassword(getString(R.string.error_verificar_identidad))
                                    Toast.makeText(applicationContext, getString(R.string.error_verificar_identidad), Toast.LENGTH_LONG).show()
                                    Log.e(TAG, getString(R.string.error_verificar_identidad))
                                }
                    }
                }
                .setNegativeButton(getString(android.R.string.cancel)) { dialogBox, id ->
                    dialogBox.cancel()
                }

        val alertDialogAndroid = builder.create()
        alertDialogAndroid.show()
    }
}
