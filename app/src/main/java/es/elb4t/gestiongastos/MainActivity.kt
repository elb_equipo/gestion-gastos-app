package es.elb4t.gestiongastos

import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.firebase.appinvite.FirebaseAppInvite
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.Query
import com.google.firebase.iid.FirebaseInstanceId
import de.hdodenhof.circleimageview.CircleImageView
import es.elb4t.gestiongastos.adapter.GrupoAdapter
import es.elb4t.gestiongastos.firebase.FireAuth.Companion.logoutUser
import es.elb4t.gestiongastos.firebase.FireDB
import es.elb4t.gestiongastos.firebase.FireDB.Companion.db
import es.elb4t.gestiongastos.firebase.FireDB.Companion.gruposPath
import es.elb4t.gestiongastos.firebase.FireDB.Companion.invitacionesPath
import es.elb4t.gestiongastos.firebase.IFirebaseMessagingService
import es.elb4t.gestiongastos.model.*
import es.elb4t.gestiongastos.utils.Connection
import es.elb4t.gestiongastos.utils.Preferences
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import java.util.*


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, GrupoAdapter.OnGroupSelectedListener {

    private val TAG: String = MainActivity::class.java.simpleName

    private var nombreUser: TextView? = null
    private var emailUser: TextView? = null
    private var imgPerfil: CircleImageView? = null

    private var mQuery: Query? = null
    private var mAdapter: GrupoAdapter? = null
    private lateinit var mGroupsRecycler: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        supportActionBar?.title = getString(R.string.title_activity_main)

        Connection(this).hayRed(drawer_layout)

        // Firestore get groups
        mQuery = db.collection(gruposPath)
                .whereArrayContains("miembros", FirebaseAuth.getInstance().currentUser?.uid ?: "")

        mGroupsRecycler = findViewById(R.id.recyclerGrupo)
        mAdapter = object : GrupoAdapter(mQuery!!, this@MainActivity) {
            override fun onDataChanged() {
                // Show/hide content if the query returns empty.
                if (itemCount == 0) {
                    mGroupsRecycler.visibility = View.GONE
                    noGroupsView.visibility = View.VISIBLE
                } else {
                    mGroupsRecycler.visibility = View.VISIBLE
                    noGroupsView.visibility = View.GONE
                }
            }

            override fun onError(e: FirebaseFirestoreException) {
                // Show a snackbar on errors
                Snackbar.make(findViewById(android.R.id.content),
                        "Error: check logs for info.", Snackbar.LENGTH_LONG).show()
            }
        }

        mGroupsRecycler.layoutManager = LinearLayoutManager(this) as RecyclerView.LayoutManager?
        mGroupsRecycler.adapter = mAdapter

        // Menu laterar
        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.setDrawerListener(toggle)
        toggle.syncState()

        val navigationView = findViewById<View>(R.id.nav_view) as NavigationView
        navigationView.setNavigationItemSelectedListener(this)

        val headerLayout = navigationView.getHeaderView(0)
        nombreUser = headerLayout.findViewById(R.id.navName) as TextView
        emailUser = headerLayout.findViewById(R.id.navEmail) as TextView
        imgPerfil = headerLayout.findViewById(R.id.imgPerfil) as CircleImageView

        fbCrearGrupo.setOnClickListener {
            crearNuevoGrupo()
        }

        if (intent.hasExtra("invitacion")) {
            FirebaseDynamicLinks.getInstance().getDynamicLink(Uri.parse(intent.getStringExtra("invitacion")))
                    .addOnSuccessListener(this) { data ->
                        if (data == null) {
                            Log.e("MAIN", "getInvitation: no data")
                            return@addOnSuccessListener
                        }

                        // Extract invite
                        val invite = FirebaseAppInvite.getInvitation(data)
                        if (invite != null) {
                            val invitationId = invite.invitationId
                            Log.e("MAIN", "Invitacion ID: $invitationId")
                            buscarInvitacion(invitationId)
                        }
                    }
                    .addOnFailureListener(this) { e ->
                        Log.w("LOGIN", "getDynamicLink:onFailure", e)
                    }
        }
    }

    override fun onBackPressed() {
        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_singout -> salir()
            R.id.nav_perfil -> startActivity(Intent(this, ProfileActivity::class.java))
            R.id.nav_notificaciones -> startActivity(Intent(this, NotificationActivity::class.java))
        }

        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onStart() {
        super.onStart()
        if (mAdapter != null) {
            mAdapter?.startListening()
        }
    }

    public override fun onStop() {
        super.onStop()
        if (mAdapter != null) {
            mAdapter?.stopListening()
        }
    }

    override fun onResume() {
        super.onResume()
        if (!Connection.redActivada) {
            Connection(this).hayRed(drawer_layout)
        } else {
            Connection(this).comprobarSiHayActualizacion()
            ActualizarDatosUsuario()
        }
    }

    fun ActualizarDatosUsuario() {
        val user = FirebaseAuth.getInstance().currentUser
        if (user != null) {
            nombreUser?.text = user.displayName ?: ""
            emailUser?.text = user.email
            val photoUrl = user.photoUrl
            Glide.with(applicationContext)
                    .load(photoUrl)
                    .error(Glide.with(applicationContext)
                            .load(R.mipmap.ic_launcher_round))
                    .into(imgPerfil!!)

            if (!Preferences(application).getBoolean("isLogued")) {
                FireDB().insertarUsuario(
                        User(user.uid, user.displayName ?: "", user.email
                                ?: "", user.photoUrl.toString(), FirebaseInstanceId.getInstance().token!!)
                )
                Preferences(application).putBoolean("isLogued", true)
            }
        }
    }

    fun salir() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.logout))
        builder.setMessage(getString(R.string.preguntaCerrarSesion))

        val positiveText = getString(android.R.string.yes)
        builder.setPositiveButton(positiveText
        ) { dialog, which ->
            logoutUser(application)
        }

        val negativeText = getString(android.R.string.cancel)
        builder.setNegativeButton(negativeText
        ) { dialog, which ->
            dialog.cancel()
        }

        val dialog = builder.create()
        // display dialog
        dialog.show()
    }

    private fun crearNuevoGrupo() {
        val layoutInflaterAndroid = LayoutInflater.from(this)
        val mView = layoutInflaterAndroid.inflate(R.layout.dialog_crear_grupo, null)
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.introduce_nombre_grupo))
        builder.setView(mView)

        val nombreGrupoDialogo = mView.findViewById(R.id.eNombreGrupoDialog) as EditText
        builder.setCancelable(false)
                .setPositiveButton("Crear") { dialog, which ->
                    FireDB().insertarGrupoGasto(this,
                            DataGrupoGastos(
                                    nombreGrupoDialogo.text.toString(),
                                    FirebaseAuth.getInstance().currentUser?.uid!!,
                                    listOf(FirebaseAuth.getInstance().currentUser?.uid!!)
                            )
                    )
                }
                .setNegativeButton(getString(android.R.string.cancel)) { dialog, which ->
                    dialog.cancel()
                }

        val dialog = builder.create()
        dialog.show()
    }

    override fun onGroupSelected(grupo: DocumentSnapshot) {
        startActivityForResult(
                Intent(applicationContext, ListadoGastosActivity::class.java)
                        .putExtra("idGrupo", grupo.id)
                        .putExtra("uidPropietario", grupo.data?.get("uidUserPropietario").toString())
                        .putExtra("descripcion", grupo.data?.get("nombre").toString())
                , 1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 1) {
            when (resultCode) {
                FireDB.RESULT_DELETE_OR_ABANDONED_GROUP -> {
                    if (!data?.getStringExtra("msg").isNullOrEmpty()) {
                        Snackbar.make(findViewById(R.id.include),
                                data?.getStringExtra("msg").toString(), Snackbar.LENGTH_LONG).show()
                    }
                }
            }
        }
    }

    fun buscarInvitacion(invitationId: String) {
        db.collection(invitacionesPath)
                .document(invitationId)
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val document = task.result
                        if (document.exists()) {
                            val invitacion = document.toObject(Invitacion::class.java)
                            Log.e(TAG, "INVI : ${invitacion?.userSendInvitacion} - ${invitacion?.descripcionGrupo} - ${invitacion?.idGrupo}")

                            val notificacion = Notification(
                                    invitationId,
                                    NotificationData(
                                            FirebaseAuth.getInstance().uid!!,
                                            getString(R.string.invitacion_unirse_titulo),
                                            String.format(getString(R.string.invitacion_unirse_msg), invitacion?.userSendInvitacion, invitacion?.descripcionGrupo),
                                            invitacion?.idGrupo!!,
                                            Random().nextInt()
                                    )
                            )

                            IFirebaseMessagingService.displayNotification(this, notificacion)

                            FireDB().eliminarInvitacion(this, invitationId)
                        }
                    }
                }
    }
}
